const path = require('path');
const ytdl = require('ytdl-core');
const express = require('express');
const bodyParser = require('body-parser');

const app = express()
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/youtube', (req, res) => {
    res.render('youtube/index', { links: '', data: []});
})

app.post('/youtube', async (req, res) => {
    const { links } = req.body;
    const urls = req.body.links.split("\r\n");
    const data = [];

    for(let url of urls) {
        const videoID = ytdl.getVideoID(url);
        const info = await ytdl.getInfo(videoID);
        const row = [];
        row['videoId'] = videoID;
        row['thumbnail'] = info.videoDetails.thumbnails[0].url;
        info.formats.forEach(format => {
            if (format.mimeType.match(/audio\/mp4/)) {
                row['audio'] = format.url;
            }
            if (format.mimeType.match(/video\/mp4/) && format.qualityLabel.match(/1080p/) && format.codecs.match(/avc/)) {
                row['1080p'] = format.url;
            }
            if (format.mimeType.match(/video\/mp4/) && format.qualityLabel.match(/720p/) && format.codecs.match(/avc/)) {
                row['720p'] = format.url;
            }
        });

        row['video'] = row['1080p'] || row['720p'];
        row['videoQuality'] = row['1080p'] ? '1080p' : '720p';
        data.push(row);
        //res.json(info.formats);
    }

    //res.json(data);
    res.render('youtube/index', { data, links });
})

app.get('/favicon.ico', function(req, res) {
    res.sendStatus(204);
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
